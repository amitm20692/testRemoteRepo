/*-----------------------------------------------------------------------------*
 * Project:   testRemoteRepo
 * File:      program.c
 * Author:    Amit Mishra (amit.m.20692+code.khazana@gmail.com)
 * 
 * Description:
 * 
 * Revision History:
 *    2018-May-20: 
 * 
 * Copyright (c) 2018 CodeKhazana
 * 
 *----------------------------------------------------------------------------*/

#include <stdio.h>

int main()
{
    printf("Learning git is fun!");
}